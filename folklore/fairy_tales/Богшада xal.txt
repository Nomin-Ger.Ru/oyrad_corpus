﻿Богшада

Кезәнә негн бәәҗ. Богшада яшл деер сууҗ, дораснь яшл богшадаг хатхад өкҗ. «Нама ода яһад хатхҗахмбч?» «Чамаг хазх, идх яманд оч келнәв»; «Яман, яман чамд хазх сән бүчр бәәнә». – «Яа, түргичн хазх бәәтхә күүкдән теҗәҗ чадҗанав». «Чамаг итх чонд оч келнәв». Чон, чон, тенд яман бәәнә, оч ид». – «Яа, түргичн итх бәәтхә, белтркүдән теҗәҗ чадҗанав». «Чамаг цокч алх адучнрт оч келнәв». Адучнрт ирәд келв: «Яа, түргичн цокҗ алх бәәтхә, адуһан геечкәт олҗ чадад эзнәсн әәҗәнәвдн. «Таниг цокх эзнттн оч келнәв». Эзнд ирв. «Эзн, эзн, оч адучнран цоктн. Теднтн адуһан геечкич». – «Яа, тедниг цокк бәәтхә, әрвңгән дааҗ чадҗанав». «Әрвңгичн шимүлх хулһнд оч келнәв. «Хулһна, хулһна, оч әрвһңгинь шим». – «Яа, әрвңг шимх бәәтхә, көяһән көялҗ чадҗанав». «Чамаг идх куушкад оч келнәв». «Куушка, куушка, тенд хулһнг бәәнә, оч бәрҗ ид». «Яа, түргичн идх бәәтхә, күүкдән теҗәҗ чадҗанав». Тадниг цокҗ алулх көвүд, күүкдт келнәв». «Көвүд, күүкд, тенд куушка бәәнә, оч цокҗ алтн». «Йа куушка цокҗ алх бәәтхә, туһлан эктән ниилүлчкәд, ээҗ аавасн әәһәд җе гиҗ йовнавдн. «Э, э таниг цокулх ээҗтн оч келнәв». Һарад йобб. Күрч ирв. «Күүкд көвүдтн туһлан эктәһинь нилүлчкиҗ, оч цоктн» – гиҗәнә. «А, а тедниг цокк бәәтхә ноосан савҗ чадҗанавдн». «А, а ноосичн таралһх түргн салькнд оч келнәв». Һарад йобб. «Түргн салькн, түргн салькн, тенд эмгчудин савҗасн ноосн бәәнә, оч тара» – гиҗәнә.
Түргн салькн ирәд, ноос тарав. Эмгчүд ай гилдәд көвүд күүкдән одад цокв. Көвүд күүкд куушкаһан цокв. Куушка хулһнан бәрҗ идв. Хулһн эзнә өрвңг шимв, эзнь адучнран цокв, адучнр чонан цокв, чон ямаһан бәрҗ идв, яман бүчрән хазв, богшада инәһә, инәһә бәәҗ геснь хаһрад үкв.

(Бичкндм эцкм келдг билә.)
Келмрч Манҗин Санҗ

Хальмг туульс. II боть. Элст: Хальмг дегтр hарhач, 1968. х. 227-228.



Бор богшада

Бор богшада нисч йовад, буян үзүр деер сууна. Буян үзүр хатхад оркна.
– Нама хатхдг чамаг яманд келҗ хазулнав! – гиһәд нисәд һарна. Ирәд: – Яман, яман! Буян үзүр хазщ өгич, – гив.
– Чини буян үзүр хазх биш, ишкән яһҗ теҗәхв гиҗәнәв, – гив.
– Тиигхлә, кишго күн, чонд оч келҗ, ишкичн идүлнәв! – гиһәд чонур нисәд һарв. – Чон, чон! Ямана ишк идҗ өгич, – гив.
– Чини ямана ишк идх биш, эврән хаана адучнрт көөгдәд түрҗәнәв, – гиҗ чон келв.
– Кишго күн, чамаг хаана адучнрт келҗ алулнав! – гиһәд, нисәд һарна. Ирәд: – Адучнр, адучнр! Чоныг нанд алҗ өгтн, – гив.
– Җили цааран, чини чон алх биш, хаандан шоодулад үкҗ йовнавидн, – гив.
– Тиигхлә, таниг хаандтн оч келҗ заклһнав! – гиһәд, нисч һарад, хаанд келв: – Хан, хан! Адучнран зактн, – гив.
– Чамд адучнр закх биш, әрвңгән дааҗ ядад үкҗәнәв! – гив.
– Әрвңгичн шимх хулһнд келнәв, – гиһәд нисч ирәд: – Хулһн, хулһн! Хаана әрвң шимҗ ас, – гив.
– Чини хаана әрвң шимх биш, көйәһән көйәлҗ яджанав! – гив.
– Тиигхлә, кишго күн, көйәһичн хамхлх күүкдт оч келнәв, – гиһәд нисч һарад, күүкдт келв: – Күүкд, хулһнын көйә хамхлҗ өгтн, – гив.
– Җили цааран, чини көйә хамхлх биш, эврән туһлан ниилүлчкәд, экнрәсн әәһәд үкҗәнәвидн! – гив.
– Тадниг экнрдтн оч муудхнав, – гиһәд экнрдтнь келв: – Эмгд, эмгд! Күүкдтн туһлан ниилүлҗ оркв, теднән застн, – гив.
– Җили цааран, чамд күүкд засх биш, ноосан яһҗ салькнас бултулҗ авхвидн гиҗәнәвидн.
– Тиигхлә, тарах салькнд оч келнәв, – гиһәд ирв. – Салькн, салькн! Тенд нег хотна эмгдин ноосинь тараҗ өгич, – гив.
Салькн ирәд, өркәрнь орад, үүдәрнь һархларн, ноосинь цугтнь тарав. Ноосан таралһулсн эмгд хордхларн, күүкдән цокв. Күүкднь хулһнын көйә хамхлв. Хулһн күүкдт хордад, хаана әрвң шимв. Хан адучнран цокв, адучнр чоныг гүвдв. Чон гүвдүлн хордад, ямана ишк идв. Ишкән идүлсн яман ядхларн, буян үзүр хазв.
Бор богшада инәһә-инәһә бәәҗ, һолһань хаһрад үкв.

Калмыцкие сказки: сборник/ на калмыцком языке. Составитель В.Д. Бадмаева. Элиста: ГУ «Издательский дом «Герел», 2009. с. 404-405.



